import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Article } from '../article/article';
import { ArticleService } from '../article/article.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article-page',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './article-page.component.html',
  styleUrl: './article-page.component.css'
})
export class ArticlePageComponent {
  article! : Article
  
constructor(private articleService : ArticleService, private route: ActivatedRoute)
{
  this.route.params.subscribe(parameters =>{
    let articleTopic = parameters[`id`]
    this.article = this.articleService.getArticleByTopic(articleTopic)
  })
}
}
