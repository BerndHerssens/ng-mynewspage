export interface Article {
    id: any;
    name: any;
    author: any;
    title: any;
    description: any;
    url: any;
    urlToImage: any;
    publishedAt: any;
    content: any;
}
