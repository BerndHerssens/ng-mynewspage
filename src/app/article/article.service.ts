import { Injectable } from '@angular/core';
import { Article } from './article';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  currentDate: Date = new Date();
  formattedDate: string = `${this.currentDate.getFullYear()}-${(this.currentDate.getMonth() + 1).toString().padStart(2, '0')}-${this.currentDate.getDate().toString().padStart(2, '0')}`;
  articles: Article[] = []
  private readonly BASE_API = "https://newsapi.org/v2/top-headlines"
  private readonly API_KEY= "apiKey=9285f55cc43b4656a0965396bb197dd3"

  constructor(private httpClient : HttpClient) { }
  
  getArticles(category: string |undefined) { //standard
    console.log(category);
    return this.httpClient.get(`${this.BASE_API}?country=us&${category!=undefined?"category="+category.toLowerCase()+"&":""}${this.API_KEY}`).pipe(
      map ((response: any) => response.articles)
    )
   //return this.httpClient.get<any>(this.urlPath)
  }

  getArticleByTopic(name: string): Article { //when looking for an article by topic/name?
    for (let article of this.articles){
      if (article.name == name)
        {return article;}
    }
    throw new Error("Could not find an article with topic " + name)
  }

  getArticleByAuthor(author: string): Article {
    for (let article of this.articles){
      if (article.author == author)
        {return article;}
    }
    throw new Error("Could not find an article with author " + author)
  }

  getNewestArticles(category: string |undefined){
    return this.httpClient.get
  }
}
