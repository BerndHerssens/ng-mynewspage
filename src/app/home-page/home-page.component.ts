import { Component } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Article } from '../article/article';
import { ArticleService } from '../article/article.service';

@Component({
  selector: 'app-home-page',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.css'
})
export class HomePageComponent {

followLink(url: string) { //we maken een methode om dan gewoon de url te volgen die we mee gekregen hebben.
  
window.open(url, `_blank`);
}
articles: Article[] = [];
category: string = "";

constructor(private route:ActivatedRoute, private articleService : ArticleService){
  this.route.params.subscribe(p=> {
    this.category = p[`category`]
    this.articleService.getArticles(this.category).subscribe(articles=> this.articles=articles);
  })
}
}


