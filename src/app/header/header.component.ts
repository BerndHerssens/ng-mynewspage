import { Component } from '@angular/core';
import { HeroComponent } from "./hero/hero.component";
import { NavbarComponent } from "./navbar/navbar.component";

@Component({
    selector: 'app-header',
    standalone: true,
    templateUrl: './header.component.html',
    styleUrl: './header.component.css',
    imports: [HeroComponent, NavbarComponent]
})
export class HeaderComponent {

}
